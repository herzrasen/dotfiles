from libqtile.config import Key, Screen, Group, Drag, Click, Match
from libqtile.command import lazy
from libqtile import layout, bar, widget, hook
import subprocess
import os

mod = "mod4"
home = os.path.expanduser('~')

nord = [
    '#2E3440',
    '#3B4252',
    '#434C5E',
    '#4C566A',
    '#D8DEE9',
    '#E5E9F0',
    '#ECEFF4',
    '#8FBCBB',
    '#88C0D0',
    '#81A1C1',
    '#5E81AC',
    '#BF616A',
    '#D08770',
    '#EBCB8B',
    '#A3BE8C',
    '#B48EAD'
];

def go_to_next_group():
    @lazy.function
    def __inner(qtile):
        index = qtile.groups.index(qtile.currentGroup)
        if index < len(qtile.groups) - 2:
            qtile.groups[index + 1].cmd_toscreen()
        else:
            qtile.groups[0].cmd_toscreen()

    return __inner


def go_to_prev_group():
    @lazy.function
    def __inner(qtile):
        index = qtile.groups.index(qtile.currentGroup)
        if index > 0:
            qtile.groups[index - 1].cmd_toscreen()
        else:
            qtile.groups[len(qtile.groups) - 2].cmd_toscreen()

    return __inner

def window_to_prev_screen():
    @lazy.function
    def __inner(qtile):
        if qtile.currentWindow is not None:
            index = qtile.screens.index(qtile.currentScreen)
            if index > 0:
                qtile.currentWindow.togroup(qtile.screens[index - 1].group.name)
            else:
                qtile.currentWindow.togroup(qtile.screens[len(qtile.screens) - 1].group.name)
    return __inner


def window_to_next_screen():
    @lazy.function
    def __inner(qtile):
        if qtile.currentWindow is not None:
            index = qtile.screens.index(qtile.currentScreen)
            if index < len(qtile.screens) - 1:
                qtile.currentWindow.togroup(qtile.screens[index + 1].group.name)
            else:
                qtile.currentWindow.togroup(qtile.screens[0].group.name)
    return __inner

keys = [
    # Switch between windows in current stack pane
    Key([mod], "k", lazy.layout.down()),
    Key([mod], "j", lazy.layout.up()),

    # Move windows up or down in current stack
    Key([mod, "control"], "k", lazy.layout.shuffle_down()),
    Key([mod, "control"], "j", lazy.layout.shuffle_up()),

    # Switch window focus to other pane(s) of stack
    Key([mod], "space", lazy.layout.next()),

    # Swap panes of split stack
    Key([mod, "shift"], "space", lazy.layout.rotate()),

    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key([mod, "shift"], "Return", lazy.layout.toggle_split()),
    Key([mod], "Return", lazy.spawn("termite")),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout()),
    Key([mod], "w", lazy.window.kill()),

    Key([mod, "control"], "r", lazy.restart()),
    Key([mod, "control"], "q", lazy.shutdown()),
    Key([mod], "r", lazy.spawn("rofi -show run")),

    Key([mod], "Left", go_to_prev_group()),
    Key([mod], "Right", go_to_next_group()),

    Key([mod, "control"], "Left", window_to_prev_screen()),
    Key([mod, "control"], "Right", window_to_next_screen()),

    # Display settings
    Key([mod], "F1", lazy.spawn(home + '/.config/screenlayout/laptop.sh')),
    Key([mod], "F2", lazy.spawn(home + '/.config/screenlayout/homeoffice.sh')),
    Key([mod], "F3", lazy.spawn(home + '/.config/screenlayout/office.sh')),

    # Volume commands
    Key([], "XF86AudioMute", lazy.spawn("amixer -c 0 -q set Master toggle")),
    Key([], "XF86AudioRaiseVolume", lazy.spawn("amixer -c 0 -q set Master 2dB+")),
    Key([], "XF86AudioLowerVolume", lazy.spawn("amixer -c 0 -q set Master 2dB-")),

    # Application shortcuts
    Key([mod], "f", lazy.spawn("firefox")),
]

groups = [
    Group(name = "1", label = "sh", matches = [Match(wm_class=["Termite"])]),
    Group(name = "2", label = "www", matches = [Match(wm_class=["Firefox"])]),
    Group(name = "3", label = "dev", matches = [Match(wm_class=["jetbrains-idea","code-oss","code"])]),
    Group(name = "4", label = "chat", matches = [Match(wm_class=["Slack"])]),
    Group(name = "5", label = "..."),
    Group(name = "6", label = "...")
]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([mod], i.name, lazy.group[i.name].toscreen()),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key([mod, "shift"], i.name, lazy.window.togroup(i.name)),
    ])

layouts = [
    layout.Max(),
    layout.Stack(num_stacks=2),
    layout.MonadTall(ratio = 0.65),
]

widget_defaults = dict(
    font='mononoki',
    fontsize=12,
    padding=2,
)
extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        top=bar.Bar(
            [
                widget.GroupBox(highlight_method = 'block', this_current_screen_border = nord[15], active = nord[6], inactive = nord[8], rounded = False, urgent_alert_method = 'block', urgent_border = nord[11]),
                widget.WindowName(foreground = nord[14]),
                widget.Sep(),
                widget.Volume(foreground = nord[6]),
                widget.Sep(),
                widget.Battery(charge_char = u'▲', discharge_char = u'▼', foreground = nord[6]),
                widget.Sep(),
                widget.Clock(format='%H:%M', foreground = nord[6]),
                widget.Sep(),
                widget.Systray(background = nord[2]),
            ],
            24,
            background = nord[2],
            opacity = 0.6
        ),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
main = None
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False

floating_layout = layout.Floating(float_rules=[
    {'wmclass': 'confirm'},
    {'wmclass': 'dialog'},
    {'wmclass': 'download'},
    {'wmclass': 'error'},
    {'wmclass': 'file_progress'},
    {'wmclass': 'notification'},
    {'wmclass': 'splash'},
    {'wmclass': 'toolbar'},
    {'wmclass': 'confirmreset'},  # gitk
    {'wmclass': 'makebranch'},  # gitk
    {'wmclass': 'maketag'},  # gitk
    {'wname': 'branchdialog'},  # gitk
    {'wname': 'pinentry'},  # GPG key password entry
    {'wmclass': 'ssh-askpass'},  # ssh-askpass
    {'wmclass': 'slack'},
    {'wmclass': 'arandr'},
    {'wmclass': 'sun-awt-X11-XDialogPeer'},
    {'wmclass': 'nm-connection-editor'},
])
auto_fullscreen = True
focus_on_window_activation = "smart"
wmname = "LG3D"

@hook.subscribe.startup
def autostart():
    subprocess.call([home + '/.config/qtile/autostart.sh'])

@hook.subscribe.screen_change
def screen_changed(qtile, ev):
    qtile.cmd_restart()
